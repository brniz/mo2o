<?php

namespace App\Controller;

use App\Service\PunkAPIService;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class BeerController extends AbstractController
{

    /**
     *
     * @var PunkApiService
     */
    private $apiService;

    public function __construct(PunkAPIService $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * @Route("/beers", name="beers", methods={"GET"})
     */
    public function beersAction(Request $request)
    {
        $groups = array();
        if (!is_null($food = $request->query->get("food", null))) {
            $groups["groups"] = "search";
        }        
        $encoders = [new JsonEncoder()];
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizers = [new ObjectNormalizer($classMetadataFactory, new CamelCaseToSnakeCaseNameConverter())];

        $serializer = new Serializer($normalizers, $encoders);

        return new JsonResponse($serializer->normalize($this->apiService->getBeers($food), 'json', $groups));
    }

    /**
     * @Route("/beers/{beer}", name="beers_beer", methods={"GET"}, requirements={"beer"="\d+"})
     */
    public function beerAction($beer)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter())];

        $serializer = new Serializer($normalizers, $encoders);

        return new JsonResponse($serializer->normalize($this->apiService->getBeer($beer), 'json'));
    }

}
