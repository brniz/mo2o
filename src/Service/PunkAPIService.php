<?php

namespace App\Service;

use App\Entity\Beer;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\json_decode;

class PunkAPIService
{

    const API_ROOT_ENDPOINT = "https://api.punkapi.com/v2/";

    public function getBeer($id)
    {
        if (is_null($beer = $this->curl(self::API_ROOT_ENDPOINT . "beers/" . $id)) || empty($beer)) {
            return null;
        }
        return new Beer($beer[0]);
    }

    public function getBeers($food = null)
    {
        $beers = array();
        $endpoint = self::API_ROOT_ENDPOINT . "beers";
        if (!is_null($food)) {
            $endpoint .= "?food=" . str_replace(" ", "_", $food);
        }
        if (is_null($beersData = $this->curl($endpoint))) {
            return null;
        }
        foreach ($beersData as $beerData) {
            $beers[] = new Beer($beerData);
        }
        return $beers;
    }

    private function curl($endpoint)
    {
        $client = new Client();
        try {
            $response = $client->request('GET', $endpoint);
        } catch (ClientException $e) {
            // ignoring error in this example
            return null;
        }
        return json_decode($response->getBody());
    }

}
