<?php

namespace App\Entity;

use Symfony\Component\Serializer\Annotation\Groups;

class Beer
{

    /**
     * @Groups("search")
     */
    protected $id;

    /**
     * @Groups("search")
     */
    protected $name;

    /**
     * @Groups("search")
     */
    protected $description;
    protected $imageUrl;
    protected $tagline;
    protected $firstBrewed;

    public function __construct($value = array())
    {
        if (!empty($value)) {
            $this->hydrate($value);
        }
    }

    public function hydrate($data)
    {
        foreach ($data as $attribut => $value) {
            $method = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $attribut)));
            if (is_callable(array($this, $method))) {
                $this->$method($value);
            }
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    public function getTagline()
    {
        return $this->tagline;
    }

    public function getFirstBrewed()
    {
        return $this->firstBrewed;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }

    public function setTagline($tagline)
    {
        $this->tagline = $tagline;
    }

    public function setFirstBrewed($firstBrewed)
    {
        $this->firstBrewed = $firstBrewed;
    }

}
