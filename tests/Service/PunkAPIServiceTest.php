<?php

namespace App\Tests\Service;

use App\Entity\Beer;
use App\Service\PunkAPIService;
use PHPUnit\Framework\TestCase;

class PunkAPIServiceTest extends TestCase
{

    public function testGetBeer()
    {
        $api = new PunkAPIService();

        $this->assertNull($api->getBeer(1000000000000000000));
        $this->assertInstanceOf(Beer::class, $api->getBeer(1));
    }

    public function testGetBeers()
    {
        $api = new PunkAPIService();

        $this->assertGreaterThan(0, count($api->getBeers()));
        $this->assertGreaterThan(0, count($api->getBeers("cucumber")));
    }

}
